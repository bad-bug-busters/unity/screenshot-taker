﻿using UnityEngine;
using UnityEditor;

public class Screenshot : EditorWindow
{
	int resWidth = Screen.width; 
	int resHeight = Screen.height;

	public Camera cam;
	int scale = 1;

	string path = "";

	[MenuItem("Tools/Screenshot Taker")]
	public static void Init()
	{
		EditorWindow editorWindow = GetWindow(typeof(Screenshot));
		editorWindow.Show();
	}

	void OnGUI()
	{
		EditorGUILayout.LabelField ("Resolution", EditorStyles.boldLabel);
		resWidth = EditorGUILayout.IntField ("Width", resWidth);
		resHeight = EditorGUILayout.IntField ("Height", resHeight);

		EditorGUILayout.Space();

		scale = EditorGUILayout.IntSlider ("Scale", scale, 1, 15);

		EditorGUILayout.HelpBox("The default mode of screenshot is crop - so choose a proper width and height. The scale is a factor " + "to multiply or enlarge the renders without loosing quality.",MessageType.None);

		EditorGUILayout.Space();
		
		GUILayout.Label ("Save Path", EditorStyles.boldLabel);

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.TextField(path,GUILayout.ExpandWidth(false));
		if(GUILayout.Button("Browse",GUILayout.ExpandWidth(false)))
			path = EditorUtility.SaveFolderPanel("Path to Save Images",path,Application.dataPath);

		EditorGUILayout.EndHorizontal();

		EditorGUILayout.HelpBox("Choose the folder in which to save the screenshots ",MessageType.None);
		EditorGUILayout.Space();

		GUILayout.Label ("Select Camera", EditorStyles.boldLabel);

		cam = EditorGUILayout.ObjectField(cam, typeof(Camera), true,null) as Camera;
		if(cam == null)
			cam = Camera.main;

		EditorGUILayout.Space();
		EditorGUILayout.BeginVertical();
		EditorGUILayout.LabelField ("Default Options", EditorStyles.boldLabel);

		if(GUILayout.Button("Set To Screen Size"))
		{
			resHeight = (int)Handles.GetMainGameViewSize().y;
			resWidth = (int)Handles.GetMainGameViewSize().x;
		}

		if(GUILayout.Button("Default Size"))
		{
			resHeight = 1080;
			resWidth = 1920;
			scale = 1;
		}

		EditorGUILayout.EndVertical();

		EditorGUILayout.Space();
		EditorGUILayout.LabelField ("Screenshot will be taken at " + resWidth*scale + " x " + resHeight*scale + " px", EditorStyles.boldLabel);

		if(GUILayout.Button("Take Screenshot",GUILayout.MinHeight(60)))
		{
			if(path == "")
			{
				path = EditorUtility.SaveFolderPanel("Path to Save Images",path,Application.dataPath);
				Debug.Log("Path Set");
				TakeHiResShot();
			}
			else
			{
				TakeHiResShot();
			}
		}

		EditorGUILayout.Space();
		EditorGUILayout.BeginHorizontal();

		if(GUILayout.Button("Open Last Screenshot",GUILayout.MaxWidth(160),GUILayout.MinHeight(40)))
		{
			if(lastScreenshot != "")
			{
				Application.OpenURL("file://" + lastScreenshot);
				Debug.Log("Opening File " + lastScreenshot);
			}
		}

		if(GUILayout.Button("Open Folder",GUILayout.MaxWidth(100),GUILayout.MinHeight(40)))
			Application.OpenURL("file://" + path);

		EditorGUILayout.EndHorizontal();

		if (takeHiResShot) 
		{
			int resWidthN = resWidth * scale;
			int resHeightN = resHeight * scale;

			cam.targetTexture = new RenderTexture(resWidthN, resHeightN, 24); ;
			RenderTexture.active = cam.targetTexture;
			var tex = new Texture2D(resWidthN, resHeightN, TextureFormat.RGB24, false);

			cam.Render();
			tex.ReadPixels(new Rect(0, 0, resWidthN, resHeightN), 0, 0);

			cam.targetTexture = null;
			RenderTexture.active = null;
			
			byte[] bytes = tex.EncodeToJPG(90);
			string filename = ScreenShotName(resWidthN, resHeightN);
			
			System.IO.File.WriteAllBytes(filename, bytes);
			Debug.Log(string.Format("Took screenshot to: {0}", filename));
			Application.OpenURL(filename);
			takeHiResShot = false;
		}
	}

	bool takeHiResShot = false;
	public string lastScreenshot = "";
	
	public string ScreenShotName(int width, int height) 
	{
		string strPath = string.Format("{0}/screen_{1}x{2}_{3}.jpg", path, width, height, System.DateTime.Now.ToString("yyyy-MM-dd_HH-mm-ss"));
		lastScreenshot = strPath;
		return strPath;
	}

	public void TakeHiResShot() {
		Debug.Log("Taking Screenshot");
		takeHiResShot = true;
	}
}
